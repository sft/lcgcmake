LCGPackage_Add(
    ROOT
    IF <VERSION> MATCHES "^v.*-patches|HEAD" THEN
      GIT_REPOSITORY http://root.cern/git/root.git GIT_TAG <VERSION>
      UPDATE_COMMAND <VOID>
    ELSE
      IF <VERSION> MATCHES "^t.*" THEN
        URL ${GenURL}/root-<VERSION>.zip
      ELSE
        URL http://root.cern.ch/download/root_v${ROOT_author_version}.source.tar.gz
      ENDIF
    ENDIF
    IF CMAKE_VERSION VERSION_LESS ROOT_MINIMAL_CMAKE_VERSION THEN 
      CMAKE_COMMAND ${CMake_home}/bin/cmake
    ENDIF
    CMAKE_ARGS -DCMAKE_BUILD_TYPE=${CMAKE_BUILD_TYPE}
               -DCMAKE_INSTALL_PREFIX=<INSTALL_DIR>
               -DCMAKE_CXX_FLAGS=-D__ROOFIT_NOBANNER
               -Dsoversion=ON
               IF NOT ${LCG_ARCH} STREQUAL i686 THEN
                -Dpython=ON
                  IF Python_native_version VERSION_GREATER 3 THEN
                    -Dpython3=ON
                    -Dpython_version=3
                  ENDIF
               ENDIF
               -Dbuiltin_lz4=ON
               -Dbuiltin_pcre=ON
               -Dbuiltin_ftgl=ON
               -Dbuiltin_afterimage=ON
               -Dbuiltin_glew=ON
               -Dcintex=ON
               -Ddavix=OFF
               -Dexceptions=ON
               -Dexplicitlink=ON
               -Dfftw3=ON
               -Dgdml=ON
               -Dgsl_shared=ON
               -Dhttp=ON
               -Dgenvector=ON
               -Dmathmore=ON
               -Dminuit2=ON
               -Dmysql=OFF
               -Dopengl=ON
               -Dpgsql=OFF
               -Dpythia6=OFF
               -Dpythia8=OFF
               -Dreflex=ON
               -Droofit=ON
               -Dssl=ON
               -Dunuran=ON
               -Dfortran=ON
               -Dxft=ON
               -Dxml=ON
               IF NOT ${LCG_ARCH} STREQUAL i686 THEN
               -Dxrootd=ON
               ELSE
               -Dxrootd=OFF
               ENDIF
               -Dzlib=ON
               -DCINTMAXSTRUCT=36000
               -DCINTMAXTYPEDEF=36000
               -DCINTLONGLINE=4096
               -Dgviz=OFF
               -Dqtgsi=OFF
               -Drfio=OFF
               -Dtable=ON
               IF NOT ${LCG_ARCH} STREQUAL i686 THEN
                -Dkrb5=ON
               ELSE
                -Dkrb5=OFF
               ENDIF
               -Dmemstat=ON
               -Dldap=ON
               -Dodbc=ON 
               -Dcxx11=ON
               -Dcastor=OFF
               -Ddcache=OFF
               -Dgfal=OFF
               -Doracle=OFF
    DEPENDS fftw GSL zlib libxml2 vdt
            IF NOT ${LCG_ARCH} STREQUAL i686 THEN
               Python
               xrootd
            ENDIF
)
