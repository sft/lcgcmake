#!/usr/bin/env bash

# The purpose of this script is to generate a LCG_contrib_*.txt file that will be attached to the release and used
# inside CVMFS and RPM build infrastructure.
# The file will be served as required during the RPM creation step:
# "name;version;hash;dirname;dependencies"
# To not trigger additional errors, the dependencies field will be served empty, even though currently all compilers
# depend on binutils. This dependency is clear for compiler RPM and will be hidden for LCG release, but during
# installation, all dependencies from compiler will be installed correctly.
# Additionally please take a note, that the hash in contrib file will be provided, but is ignored in RPMs.

# Detect compiler

# If CC is not defined try to use gcc
if [ ! $CC ]; then
    echo CC env variable not set!! Using gcc 1>&2
    export CC=$(which gcc)
fi
comp=$(${CC} --version | head -1)

# Expecting first value be the name
compname=$(echo $comp | cut -f1 -d' ')

# Get version
if [ "$compname" = gcc ]; then
    # Expected for gcc: gcc (.*) $compvers
    compvers=$(echo $comp | sed "s/(.*) //" | cut -f2 -d' ')
elif [ "$compname" = clang -o "$compname" = Apple ]; then
    # Expected for clang: .* version $compvers
    compvers=$(echo $comp | \grep -Eo "version [^ ]+" | cut -f2 -d' ')
fi

# Since 2018-08-08 ( contrib compilation with lcgcmake ) the compiler should contain .buildinfo_$name
# Try to locate .buildinfo file
if [ $RESOLVE_REALPATH ]; then
    searchpath=$(readlink -f ${CC})
else
    searchpath=$CC
fi
until ls ${searchpath}/.buildinfo_* 1> /dev/null 2>&1; do
    if [ ${searchpath} = / ]; then
        break
    fi
    searchpath=$(dirname $searchpath)
done
pkghash=$(cat ${searchpath}/.buildinfo_* 2>/dev/null | \grep -Eo "HASH: [a-z0-9]+" | cut -c7- )

# Before 2018-08-08 ( manually installed to cvmfs, does not contain any information)
# Guessing the path
if [ $searchpath = / ]; then
    searchpath=$(echo $CC | grep -o "[^/]\+/[^/]\+/x86_64-\(slc6\|centos7\)") 
fi

# Native
# Native compiler has no hash and does not specify the path to the compiler

# Print information in required format 
if [ "$searchpath" != / ]; then
    echo "$compname;$compvers;$pkghash;$(echo $searchpath | rev | cut -f1-3 -d'/' | rev);"
else
    echo "$compname;$compvers;;;"
fi

