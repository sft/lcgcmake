set(LCG_PYTHON_VERSION 3)
include(heptools-dev4)
SET(LHCB_HEPMC 3)

# Align with 106b https://its.cern.ch/jira/browse/SPI-2713
LCG_AA_project(DD4hep                  01.31                                    )
LCG_AA_project(ROOT                    6.32.10                                  )
LCG_external_package(Boost             1.86.0                                   )
LCG_external_package(hepmc3            3.2.7                                    )

LCG_remove_package(HepMC)
LCG_remove_package(cepgen)
LCG_remove_package(hepmcanalysis)

SET(LHCB_JSON_FILE https://gitlab.cern.ch/lhcb-core/rpm-recipes/-/raw/master/LHCBEXTERNALS/dev4lhcb.json)

include(heptools-lhcbsetup)

# generator versions from LHCB_8
LCG_external_package(lhapdf            6.2.3p1              ${MCGENPATH}/lhapdf author=6.2.3 usecxxstd=1 )
LCG_external_package(photos++          3.56.lhcb1           ${MCGENPATH}/photos++ author=3.56 hepmc=${LHCB_HEPMC} )
LCG_external_package(pythia6           427.2.lhcb           ${MCGENPATH}/pythia6 author=6.4.27 hepevt=200000 )
LCG_external_package(tauola++          1.1.6b.lhcb          ${MCGENPATH}/tauola++ author=1.1.6b hepmc=${LHCB_HEPMC})
LCG_external_package(crmc              2.0.1p5              ${MCGENPATH}/crmc  author=2.0.1 hepmc=${LHCB_HEPMC}   )
LCG_external_package(starlight         r313                 ${MCGENPATH}/starlight )
LCG_external_package(madgraph5amc      2.9.3.atlas1         ${MCGENPATH}/madgraph5amc author=2.9.3)
LCG_external_package(pythia8           244.lhcb4            ${MCGENPATH}/pythia8 author=244 )
LCG_user_recipe(
  superchic
  URL https://lcgpackages.web.cern.ch/tarFiles/sources/MCGeneratorsTarFiles/superchic-v<superchic_<NATIVE_VERSION>_author>.tar.gz
  CMAKE_ARGS -DCMAKE_INSTALL_PREFIX=<INSTALL_DIR>
               -DCMAKE_BUILD_TYPE=${CMAKE_BUILD_TYPE}
               -DLHAPDF_DIR=${lhapdf_home}
               -DSUPERCHIC_ENABLE_TESTS=OFF
               "-DCMAKE_Fortran_FLAGS=-ffree-line-length-none"
  DEPENDS lhapdf
)
LCG_external_package(superchic         5.1                  ${MCGENPATH}/superchic   author=5.1 )
LCG_external_package(rivet             3.1.10               ${MCGENPATH}/rivet yoda=1.9.10 hepmc=${LHCB_HEPMC})
LCG_external_package(yoda              1.9.10               ${MCGENPATH}/yoda)
LCG_external_package(thepeg            2.2.3                ${MCGENPATH}/thepeg hepmc=${LHCB_HEPMC})
LCG_external_package(herwig3           7.2.3p2              ${MCGENPATH}/herwig++  thepeg=2.2.3 madgraph=2.9.3.atlas1 openloops=2.1.1 lhapdf=6.2.3 author=7.2.3 hepmc=${LHCB_HEPMC})

LIST(APPEND LHCB_TOP_PACKAGES superchic)
LCG_top_packages(${LHCB_TOP_PACKAGES})
