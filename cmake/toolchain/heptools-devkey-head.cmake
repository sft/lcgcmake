#---List of externals
set(LCG_PYTHON_VERSION 3)
include(heptools-devkey)

# Other
LCG_external_package(fcalclusterer                           HEAD GIT=https://github.com/FCalSW/FCalClusterer.git)
LCG_external_package(opendatadetector                        HEAD GIT=https://gitlab.cern.ch/acts/OpenDataDetector.git)


# Key4hep
LCG_external_package(cldconfig                               HEAD GIT=https://github.com/key4hep/CLDConfig.git)
LCG_external_package(EDM4hep                                 HEAD GIT=https://github.com/key4hep/EDM4hep.git)
LCG_external_package(k4clue                                  HEAD GIT=https://github.com/key4hep/k4Clue.git)
LCG_external_package(k4edm4hep2lcioconv                      HEAD GIT=https://github.com/key4hep/k4EDM4hep2LcioConv.git)
LCG_external_package(k4fwcore                                HEAD GIT=https://github.com/key4hep/k4FWCore.git)
LCG_external_package(k4generatorsconfig                      HEAD GIT=https://github.com/key4hep/k4GeneratorsConfig.git)
LCG_external_package(k4geo                                   HEAD GIT=https://github.com/key4hep/k4geo.git)
LCG_external_package(k4marlinwrapper                         HEAD GIT=https://github.com/key4hep/k4MarlinWrapper.git)
LCG_external_package(k4_project_template                     HEAD GIT=https://github.com/key4hep/k4-project-template.git)
LCG_external_package(k4reco                                  HEAD GIT=https://github.com/key4hep/k4Reco.git)
LCG_external_package(k4rectracker                            HEAD GIT=https://github.com/key4hep/k4RecTracker.git)
LCG_external_package(k4simdelphes                            HEAD GIT=https://github.com/key4hep/k4SimDelphes.git)

# iLCSoft
LCG_external_package(ced                                     HEAD GIT=https://github.com/iLCSoft/CED.git)
LCG_external_package(cedviewer                               HEAD GIT=https://github.com/iLCSoft/CEDViewer.git)
LCG_external_package(clicperformance                         HEAD GIT=https://github.com/iLCSoft/CLICPerformance.git)
LCG_external_package(clupatra                                HEAD GIT=https://github.com/iLCSoft/Clupatra.git)
LCG_external_package(conddbmysql                             HEAD GIT=https://github.com/iLCSoft/CondDBMySQL.git)
LCG_external_package(conformaltracking                       HEAD GIT=https://github.com/iLCSoft/ConformalTracking.git)
LCG_external_package(ddkaltest                               HEAD GIT=https://github.com/iLCSoft/DDKalTest.git)
LCG_external_package(ddmarlinpandora                         HEAD GIT=https://github.com/iLCSoft/DDMarlinPandora.git)
LCG_external_package(forwardtracking                         HEAD GIT=https://github.com/iLCSoft/ForwardTracking.git)
LCG_external_package(garlic                                  HEAD GIT=https://github.com/iLCSoft/Garlic.git)
LCG_external_package(gear                                    HEAD GIT=https://github.com/iLCSoft/GEAR.git)
LCG_external_package(ilcutil                                 HEAD GIT=https://github.com/iLCSoft/ILCUtil.git)
LCG_external_package(ildperformance                          HEAD GIT=https://github.com/iLCSoft/ILDPerformance.git)
# LCG_external_package(kaldet                                  HEAD GIT=https://github.com/iLCSoft/KalDet.git)
LCG_external_package(k4simgeant4                             HEAD GIT=https://github.com/key4hep/k4SimGeant4.git)
LCG_external_package(kaltest                                 HEAD GIT=https://github.com/iLCSoft/KalTest.git)
LCG_external_package(kitrack                                 HEAD GIT=https://github.com/iLCSoft/KiTrack.git)
LCG_external_package(kitrackmarlin                           HEAD GIT=https://github.com/iLCSoft/KiTrackMarlin.git)
LCG_external_package(lccd                                    HEAD GIT=https://github.com/iLCSoft/LCCD.git)
LCG_external_package(lcfiplus                                HEAD GIT=https://github.com/lcfiplus/LCFIPlus.git)
# LCG_external_package(lcfivertex                              HEAD GIT=https://github.com/iLCSoft/LCFIVertex.git)
LCG_external_package(LCIO                                    HEAD GIT=https://github.com/iLCSoft/LCIO.git)
LCG_external_package(lctuple                                 HEAD GIT=https://github.com/iLCSoft/LCTuple.git)
LCG_external_package(marlin                                  HEAD GIT=https://github.com/iLCSoft/Marlin.git)
LCG_external_package(marlindd4hep                            HEAD GIT=https://github.com/iLCSoft/MarlinDD4hep.git)
LCG_external_package(marlinfastjet                           HEAD GIT=https://github.com/iLCSoft/MarlinFastjet.git)
LCG_external_package(marlinkinfit                            HEAD GIT=https://github.com/iLCSoft/MarlinKinfit.git)
LCG_external_package(marlinkinfitprocessors                  HEAD GIT=https://github.com/iLCSoft/MarlinKinfitProcessors.git)
LCG_external_package(marlinmlflavortagging                   HEAD GIT=https://gitlab.desy.de/ilcsoft/MarlinMLFlavorTagging.git)
LCG_external_package(marlinReco                              HEAD GIT=https://github.com/iLCSoft/MarlinReco.git)
LCG_external_package(marlintrk                               HEAD GIT=https://github.com/iLCSoft/MarlinTrk.git)
LCG_external_package(marlintrkprocessors                     HEAD GIT=https://github.com/iLCSoft/MarlinTrkProcessors.git)
LCG_external_package(marlinutil                              HEAD GIT=https://github.com/iLCSoft/MarlinUtil.git)
LCG_external_package(overlay                                 HEAD GIT=https://github.com/iLCSoft/Overlay.git)
LCG_external_package(physsim                                 HEAD GIT=https://github.com/iLCSoft/PhysSim.git)
LCG_external_package(raida                                   HEAD GIT=https://github.com/iLCSoft/RAIDA.git)
LCG_external_package(SIO                                     HEAD GIT=https://github.com/iLCSoft/SIO.git)

# PandoraPFA
# LCG_external_package(larcontent                         04.11.02)
# LCG_external_package(lccontent                          03.01.06)
LCG_external_package(pandoraanalysis                         HEAD GIT=https://github.com/PandoraPFA/LCPandoraAnalysis.git)
# LCG_external_package(pandoramonitoring                  03.06.00)
# LCG_external_package(pandorapfa                         04.11.02)
# LCG_external_package(pandorasdk                         03.04.02)

# AIDASoft
LCG_external_package(aidatt                                  HEAD GIT=https://github.com/AIDASoft/aidaTT.git)
LCG_external_package(podio                                   HEAD GIT=https://github.com/AIDASoft/podio.git)
LCG_external_package(DD4hep                                  HEAD GIT=https://github.com/AIDASoft/DD4hep.git)

# HEP-FCC
LCG_external_package(dual_readout                            HEAD GIT=https://github.com/HEP-FCC/dual-readout.git)
LCG_external_package(fcc_config                              HEAD GIT=https://github.com/HEP-FCC/FCC-config.git)
LCG_external_package(fccdetectors                            HEAD GIT=https://github.com/HEP-FCC/FCCDetectors.git)
LCG_external_package(fccsw                                   HEAD GIT=https://github.com/HEP-FCC/FCCSW.git)
LCG_external_package(k4gen                                   HEAD GIT=https://github.com/HEP-FCC/k4Gen.git)

# Other packages thate require checks

# Check in dev-base for onnxruntime
if(${LCG_COMP}${LCG_COMPVERS} MATCHES "gcc1[1234]" )
  LCG_external_package(ddfastshowerml                        HEAD GIT=https://gitlab.desy.de/ilcsoft/ddfastshowerml.git)
  LCG_external_package(k4reccalorimeter                      HEAD GIT=https://github.com/HEP-FCC/k4RecCalorimeter.git)
endif()


if(NOT ${LCG_OS} MATCHES mac)
  LCG_external_package(fccanalyses                           HEAD GIT=https://github.com/HEP-FCC/FCCAnalyses.git) # Until https://github.com/HEP-FCC/FCCAnalyses/issues/417 is fixed
endif()
