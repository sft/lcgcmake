
#---There are a number of packages not supported with ARM64--------

LCG_remove_package(coffea)
LCG_remove_package(cpymad)
LCG_remove_package(pystan)
LCG_remove_package(httpstan)
LCG_remove_package(dcap)
LCG_remove_package(gfal)
LCG_remove_package(srm_ifce)

LCG_remove_package(R)

# Remove polars which calling jemalloc fails
LCG_remove_package(polars)

# hadoop and dependents
LCG_remove_package(hadoop)
LCG_remove_package(hadoop_xrootd)
LCG_remove_package(spark)

# gomacro and dependents
LCG_remove_package(gomacro)
LCG_remove_package(gophernotes)

#mcfm builds many internal packages for static linking which have a problem with aarm64
LCG_remove_package(mcfm)
LCG_remove_package(nlox)

# needs onnxruntime
LCG_remove_package(hls4ml)
# not compatible with this architecture
LCG_remove_package(protobuf2)

LCG_remove_package(kokkos)
LCG_remove_package(pepper_kokkos)

# incredibly old auto-config (2001)
LCG_remove_package(xqilla)
LCG_remove_package(omniorb)

# end python packages

#---Overwrites of versions ----------(because ARM) ----------------
LCG_external_package(blas      ${blas_native_version} target=ARMV8)
LCG_external_package(oracle    19.10.0.0.0 )

if(${LCG_OS}${LCG_OSVERS} MATCHES centos7)
  # newer version does not compile on centos7 aarch64, glibc-headers too old
  LCG_external_package(gperftools 2.9.1 )

  # itk_core is not available for centos7 aarch64
  LCG_remove_package(itk                 )
  LCG_remove_package(itk_core            )
  LCG_remove_package(itk_io              )
  LCG_remove_package(itk_filtering       )
  LCG_remove_package(itk_meshtopolydata  )
  LCG_remove_package(itk_numerics        )
  LCG_remove_package(itk_registration    )
  LCG_remove_package(itk_segmentation    )
  LCG_remove_package(itkwidgets          )

endif()
