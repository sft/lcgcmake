#---List of externals
set(LCG_PYTHON_VERSION 3)
include(heptools-dev3)

LCG_external_package(ROOT                          6.32.06) # 6.30.08

###LCG_external_package(uuid                          2.27.1.alice) # taken from the system here
#LCG_external_package(Geant4                        10.7.3)
#LCG_external_package(HepMC                         2.06.09.alice)

## Packages we already have in the stack with different versions
#LCG_external_package(XercesC                       3.2.2)
#LCG_external_package(apfel                         3.0.6)
#LCG_external_package(evtgen                        R01-06-00-ship)
#LCG_external_package(flatbuffers                   1.11.0)
#LCG_external_package(fmt                           7.1.0)
#LCG_external_package(lhapdf                        6.5.3.snd)
#LCG_external_package(photos++                      3.64)
#LCG_external_package(pythia6                       6.4.28.snd)
#LCG_external_package(pythia8                       8230.ship)
#LCG_external_package(tauola++                      1.1.5.ship)
#LCG_external_package(zeromq                        4.3.5)
#LCG_external_package(log4cpp                       1b9f8f7c031d6947c7468d54bc1da4b2f414558d)
# LCG_external_package(gtest                    1.8.0) # googletest

# packages we do not yet have in the stack
# LCG_external_package(FairCMakeModules              1.0.0)
# LCG_external_package(FairLogger                    1.9.0)
# LCG_external_package(FairMQ                        1.4.38)
# LCG_external_package(FairRoot                      18.6.10
# LCG_external_package(alibuild-recipe-tools         0.2.2)
#LCG_external_package(asiofi                        0.5.1) # depends on FairCMakeModules

LCG_external_package(geant4_vmc                    6.6.2) #5.4
LCG_external_package(genfit                        2.0.5) # main
LCG_external_package(vmc                           2.0) #1.1.p1
LCG_external_package(asio                          1.30.2)
# LCG_external_package(genie                         3.04.02)
LCG_external_package(ofi                           1.22.0) # 1.14.0


LCG_external_package(vgm                           5.3)

LCG_top_packages(evtgen #FairCMakeModules FairLogger FairMQ FairRoot alibuild-recipe-tools asiofi GENIE
  Geant4 geant4_vmc genfit HepMC photos++ ROOT tauola++ uuid vmc xrootd XercesC zeromq apfel asio flatbuffers fmt gtest lhapdf ofi pythia8 pythia6 vgm log4cpp scipy numpy matplotlib torch torchvision torch_cluster torch_scatter torch_geometric torch_sparse)
