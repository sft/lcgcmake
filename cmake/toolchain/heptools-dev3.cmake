#---List of externals
set(LCG_PYTHON_VERSION 3)
include(heptools-dev-base)

#---Additional External packages------(Generators)-----------------
include(heptools-dev-generators)

if(LCG_OS MATCHES mac AND LCG_OSVERS VERSION_EQUAL 12)
  # later versions of ROOT do not work on macos12
  LCG_AA_project(ROOT  6.32.04)
else()
  LCG_external_package(ROOT         HEAD   GIT=http://root.cern/git/root.git)
endif()

LCG_external_package(hepmc3       HEAD   GIT=https://gitlab.cern.ch/hepmc/HepMC3.git )

LCG_external_package(DD4hep        master   GIT=https://github.com/AIDASoft/DD4hep.git   )

if(${LCG_OS}${LCG_OSVERS} MATCHES el|centos|ubuntu)
  # Gaudi needs c++20 now, only for gcc13 and clang (16)
  if(((${LCG_COMP} MATCHES gcc) AND (${LCG_COMPVERS} GREATER_EQUAL 13)) OR (${LCG_COMP} MATCHES clang))
    LCG_external_package(Gaudi master   GIT=https://gitlab.cern.ch/gaudi/Gaudi.git)
  endif()
endif()


#---Apple MacOS special removals and overwrites--------------------
include(heptools-macos)

if(LCG_ARCH MATCHES "aarch64")
  include(heptools-devARM)
endif()

# Requires a fix in DD4hep
LCG_remove_package(acts)
