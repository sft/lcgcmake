#!/bin/bash

# Run DD4hep tests from the build directory
# run-dd4hep-tests.sh <local-prefix>  <platform>  <build-dir> <dd4hep_version>

PREFIX=$1
PLATFORM=$2
BINARY_DIR=$3
VERSION=$4

# where is lcgenv?
if [ -z "$LCGENV" ]; then
  if command -v lcgenv &>/dev/null; then
     LCGENV=`command -v lcgenv`
  elif [ -x /cvmfs/sft.cern.ch/lcg/releases/lcgenv/latest/lcgenv ]; then
     LCGENV=/cvmfs/sft.cern.ch/lcg/releases/lcgenv/latest/lcgenv
  fi
fi

# how many cores?
CPUS=`grep -c ^processor /proc/cpuinfo`   

# set current directory and environment
cd ${BINARY_DIR}
eval "`$LCGENV -p ${PREFIX} ${PLATFORM} DD4hep ${VERSION}`"
env | sort | sed 's/:/:?     /g' | tr '?' '\n'

ctest --version
ctest -j${CPUS} --output-on-failure
