#!/bin/bash -x
# Using the following environment variables:
#  BUILDMODE: 'nightly' or 'release' 
#  LCG_VERSION: Stack version
#  PLATFORM: Platform
#  WORKSPACE: Jenkins workspace

REPOSITORY='sft-nightlies.cern.ch'
weekday=`date +%a`
cvmfs_user=cvsft-nightlies
sudo -i -u ${cvmfs_user}<<EOF
shopt -s nocasematch

export COMPILER=${COMPILER}
export LCG_VERSION=${LCG_VERSION}
export weekday=${weekday}
export NIGHTLY_MODE=1

attempt=1
while [ \$attempt -le 60 ]; do
    echo "Attempt \$attempt of 60..."
    if [[ -f "/cvmfs/${REPOSITORY}/lcg/nightlies/${LCG_VERSION}/$weekday/isDone-${PLATFORM}" ]] && [[ \$(date -r "/cvmfs/${REPOSITORY}/lcg/nightlies/${LCG_VERSION}/$weekday/isDone-${PLATFORM}" +%Y-%m-%d) == \$(date +%Y-%m-%d) ]]; then
        echo "File found and is from today."
        break
    elif [[ -f "/cvmfs/${REPOSITORY}/lcg/nightlies/${LCG_VERSION}/$weekday/isDone-unstable-${PLATFORM}" ]] && [[ \$(date -r "/cvmfs/${REPOSITORY}/lcg/nightlies/${LCG_VERSION}/$weekday/isDone-unstable-${PLATFORM}" +%Y-%m-%d) == \$(date +%Y-%m-%d) ]]; then
        echo "Unstable file found and is from today."
        break
    else
        echo "File not found or not from today, waiting for 10 seconds..."
        sleep 10
        ((attempt++))
    fi
done

if [ -f "/cvmfs/${REPOSITORY}/lcg/nightlies/${LCG_VERSION}/$weekday/isDone-${PLATFORM}" ] && [ xtrue = "x${VIEWS_CREATION}" ]; then
    echo "The installation of the nightly is completed with all packages, let's go for the view creation"

    cvmfs_server transaction -t 60 ${REPOSITORY}/lcg/views/${LCG_VERSION}/$weekday/$PLATFORM

    $WORKSPACE/lcgcmake/cmake/scripts/create_lcg_view.py -l /cvmfs/${REPOSITORY}/lcg/nightlies/ -r ${LCG_VERSION} -p $PLATFORM -d -B /cvmfs/${REPOSITORY}/lcg/views/${LCG_VERSION}/$weekday/$PLATFORM --conflict pytimber

    if [ "\$?" == "0" ]; then
        cd /home
        cvmfs_server publish ${REPOSITORY}/lcg/views/${LCG_VERSION}/${weekday}/${PLATFORM}
        echo "View created, updating latest symlink"

        # timeout because these transactions would block each other
        cvmfs_server transaction -t 3000 ${REPOSITORY}/lcg/views/${LCG_VERSION}/latest

        rm -f /cvmfs/${REPOSITORY}/lcg/views/${LCG_VERSION}/latest/$PLATFORM
        cd /cvmfs/${REPOSITORY}/lcg/views/${LCG_VERSION}/latest
        ln -s ../${weekday}/${PLATFORM}

        cd /home
        cvmfs_server publish ${REPOSITORY}/lcg/views/${LCG_VERSION}/latest
    else
        echo "There was an error creating view, not updating latest symlink"
        cd /home
        cvmfs_server abort -f ${REPOSITORY}/lcg/views/${LCG_VERSION}/${weekday}/${PLATFORM}
        exit 1
    fi
elif [ -f "/cvmfs/${REPOSITORY}/lcg/nightlies/${LCG_VERSION}/$weekday/isDone-unstable-${PLATFORM}" ]; then
    # check the creation date and time of the fail for debugging
    ls -l /cvmfs/${REPOSITORY}/lcg/nightlies/${LCG_VERSION}/$weekday/isDone-unstable-${PLATFORM}
    echo "The installation has not been completed and we do not create the view"
else
    if [ xtrue = "x${VIEWS_CREATION}" ]; then
        echo "VIEWS_CREATION is set to true."
    else
        echo "VIEWS_CREATION is not set true."
    fi
    if [ -f "/cvmfs/${REPOSITORY}/lcg/nightlies/${LCG_VERSION}/$weekday/isDone-${PLATFORM}" ]; then
        echo "The file /cvmfs/${REPOSITORY}/lcg/nightlies/${LCG_VERSION}/$weekday/isDone-${PLATFORM} exists."
    else
        echo "The file /cvmfs/${REPOSITORY}/lcg/nightlies/${LCG_VERSION}/$weekday/isDone-${PLATFORM} does not exist."
    fi
fi

EOF
