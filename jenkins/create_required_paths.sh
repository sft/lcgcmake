#!/bin/bash

# Usage: ./create_required_paths.sh <deepestDir> <path1> [<path2> ...]
# This script creates the provided paths using the already existing deepest subpath

DEEPEST_DIR="$1"
shift
TARGET_DIRS=("$@")

# Remove the /cvmfs/ prefix from the deepest directory for the transaction lease path
DEEPEST_DIR=${DEEPEST_DIR#/cvmfs/}

sudo -i -u cvsft-nightlies<<EOF
shopt -s nocasematch

cvmfs_server transaction -t 60 ${DEEPEST_DIR}
retVal=\$?
if [ "\$retVal" != "0" ]; then
    echo "Error starting transaction"
    exit 1
fi

# Create the target directories
for TARGET_DIR in "${TARGET_DIRS[@]}"; do
    mkdir -p \${TARGET_DIR}
    retVal=\$?
    if [ "\$retVal" != "0" ]; then
        echo "Error creating target directory: \${TARGET_DIR}"
        cvmfs_server abort -f ${DEEPEST_DIR}
        exit 1
    fi
done

cd /home
cvmfs_server publish ${DEEPEST_DIR}
EOF
