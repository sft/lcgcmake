#!/usr/bin/env python3
"""
This script determines a list of packages which have to be deleted, and a list of packages which have to be installed.
It performs the following main tasks:

1. Retrieves a list of available tarfiles from a summary text file located at a specified URL.
2. Compares existing installations against the tarfiles list to determine which packages need to be deleted or kept based on the version numbers.
3. Identifies packages to be installed by parsing build information from tarfiles.
4. Veto packages based on certain criteria (like being in a blacklist or having specific tags).
5. Generates a JSON file listing packages to install and paths to delete.

Usage:
  The script is intended to be called by the Jenkinsfile-install-latest script.

  Example:
    lcgcmake/jenkins/cvmfs_install_latest/get_package_list_for_latest.py --url https://lcgpackages.web.cern.ch/tarFiles/latest \
                                                                          --platform ${PLATFORM} \
                                                                          --prefix /cvmfs/sft-nightlies.cern.ch/lcg/latest
"""



import os, sys, tarfile, glob, re, logging, json, time
import argparse

FORMAT = '%(asctime)s %(levelname)5s %(message)s'
logging.basicConfig(stream=sys.stdout, level=logging.DEBUG, format=FORMAT)
LOG = logging.getLogger("IBR")

import urllib.request as urllib2

from collections import namedtuple
sys.path.append(os.path.join(os.path.dirname(__file__), '../../'))
from cmake.scripts.buildinfo2json import parse

def tarname_from_buildinfo(buildinfo):
  with open(buildinfo, 'r') as f:
    d = parse(f.read())
  return d['NAME']+'-'+d['VERSION']+'_'+d['HASH']+'-'+d['PLATFORM']+'.tgz'

blacklist = ['ROOT']

def packageVeto(packages):
  """We loop through all packages and blacklist until there is no change. Returns a list of vetoed packages."""
  vetoed = [p.name for p in packages if p.name.split('-')[0] in blacklist and any(x in p.name for x in ['patches','HEAD','master'])]
  oldVetoed = []
  counter = 0
  while oldVetoed != vetoed:
    LOG.info("Iteration %s, we have %s vetoed packages", counter, len(vetoed))
    counter += 1
    oldVetoed = list(vetoed)
    vetoed = [p.name for p in packages if p.name in vetoed or any(d in vetoed for d in p.depends)]
    # LOG.info("New vetos: %s", vetoed)
  LOG.info("Done vetoing: we have %s vetoed packages", len(vetoed))
  return vetoed


#---get_package_lists----------------------------------------------------------
def get_package_lists(url, prefix, platform):
  #--  ${url}/summary-${platform}.txt
  summary = os.path.join(url, 'summary-'+ platform + '.txt')
  LOG.info("Looking for summary file: %s", summary)
  try:
    resp = urllib2.urlopen(summary)
    tarfiles = resp.read().decode('utf-8').split('\n')
  except urllib2.HTTPError as detail:
    LOG.error('Error downloading %s : %s', summary, detail)
    sys.exit(1)
  except Exception as detail:
    LOG.error('Unexpected error: %s', detail)
    sys.exit(1)
  LOG.info("Successfully read summary file")
  LOG.debug("We have %s tarfiles in the summary", len(tarfiles))
  for index, tf in enumerate(tarfiles, start=1):
    LOG.debug("(%s/%s) %s", index, len(tarfiles), tf)

  #---create list of packages to delete---------------------------------------------
  paths_to_delete = []
  LOG.info("Cleaning installations")
  LOG.info("Looking for buildinfo: %s", prefix)
  start_time = time.time()
  arch, osys, comp, buildtype = platform.split('-')
  all_platforms = [platform, f"{arch}+sse3-{osys}-{comp}-{buildtype}", f"{arch}+avx2+fma-{osys}-{comp}-{buildtype}"]
  buildinfos = [file for platform in all_platforms for file in glob.glob(os.path.join(prefix, '*', '*', platform,'.buildinfo_*.txt'))]
  buildinfos += [file for platform in all_platforms for file in glob.glob(os.path.join(prefix, 'MCGenerators','*','*', platform,'.buildinfo_*.txt'))]
  LOG.info("Time to find buildinfos: %s", time.time() - start_time)
  LOG.info("Found buildinfo: %s", buildinfos)
  for index, info in enumerate(buildinfos, start=1):
    directory = os.path.dirname(info)
    if tarname_from_buildinfo(info) in tarfiles:
      LOG.info('(%s/%s) Keeping %s', index, len(buildinfos), directory)
    elif tarname_from_buildinfo(info) not in tarfiles:
      paths_to_delete.append(directory)
      LOG.info('(%s/%s) Deleting %s in the next stage', index, len(buildinfos), directory)

  package = namedtuple('package', ['tarfile', 'directory', 'name', 'depends'])
  packages = []

  #---Loop over all tarfiles----------------------------------------------------
  LOG.info("Looping over all %s tarfiles", len(tarfiles))
  for index, file in enumerate(tarfiles, start=1):
    if (file == '' or 'summary' in file or file.startswith('.sys.a#.v')):  # filter eos artifacts
      LOG.info("(%s/%s) Skipping summary or artifact: %s", index, len(tarfiles), file)
      continue
    #---Obtain the hash value
    try: 
      urltarfile = os.path.join(url, file)
      hash =  os.path.splitext(file)[0].split('-')[-5].split('_')[-1]
    except:
      LOG.info("(%s/%s) Binary tarfile name %r ill-formed", index, len(tarfiles), file)
      continue
    #---Open the remote tarfiles to get the true dirname and version------------
    try:
      LOG.info("(%s/%s) Opening tarfile %s", index, len(tarfiles), urltarfile)
      resp = urllib2.urlopen(urltarfile)
      tar = tarfile.open(fileobj=resp, mode='r|gz', errorlevel=1)
      dirname, version = os.path.split(tar.next().name)
    except urllib2.HTTPError as detail:
      LOG.error('Error accessing %s : %s', urltarfile, detail)
      continue
    except tarfile.ReadError as detail:
      LOG.error('Error untaring %s : %s', urltarfile, detail)
      continue
    except:
      LOG.error('Unexpected error: %s', sys.exc_info()[0])
      sys.exit(1)

    platform_from_tarfile = urltarfile.split('/')[-1].split('-', 2)[-1].split('.')[0]
    targetdir = os.path.join(prefix, dirname, version + '-' + hash, platform_from_tarfile)
    if not os.path.exists(targetdir) or targetdir in paths_to_delete:
      #  Get dependencies information from tarfile to be installed
      resp = urllib2.urlopen(urltarfile)
      tar = tarfile.open(fileobj=resp, mode='r|gz', errorlevel=1)
      for member in tar:
        if member.isfile() and re.search('.buildinfo_.*\.txt', member.name):
          buildinfo = tar.extractfile(member).read().decode('utf-8')
          break
      p = parse(buildinfo)
      packages.append(package(tarfile=urltarfile, \
                              directory=targetdir,\
                              name=p['NAME']+'-'+p['VERSION']+'-'+p['HASH'],\
                              depends=p['DEPENDS']))
      
  LOG.info("Vetoing packages")
  vetoed = packageVeto(packages)
  packages = [p for p in packages if p.name not in vetoed or print(f"Skipping vetoed package: {p.name}") is not None]
  
  # Serialize the namedtuples into a list of dictionaries
  packages_to_install = [pkg._asdict() for pkg in packages]

  # Prepare the final dictionary to serialize
  data_to_serialize = {
      'packages_to_install': packages_to_install,
      'paths_to_delete': paths_to_delete
  }

  # Writing to JSON file
  with open('package_list_for_latest.json', 'w') as f:
      json.dump(data_to_serialize, f)


#---Main program----------------------------------------------------------------
if __name__ == '__main__':

  #---Parse the arguments---------------------------------------------------------
  parser = argparse.ArgumentParser()
  parser.add_argument('--url', dest='url', help='URL of the binary cache repository', required=True)
  parser.add_argument('--prefix', dest='prefix', help='prefix to the installation', required=True)
  parser.add_argument('--platform', dest='platform', help='select a given platform', required=True)
  
  args = parser.parse_args()
  LOG.debug("Parsed parameters")
  LOG.debug(f"Retrieving package installation/deletion information: url=%s, prefix=%s, platform=%s", args.url, args.prefix, args.platform)
  get_package_lists(args.url, args.prefix, args.platform)
