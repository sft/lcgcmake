# How to build and install a LCG release for MacOS

## Build the release on a MacOS mode and upload binary tarfiles to EOS

1. Clone the lcgcmake repository and add the `lcgcmake/bin` into the PATH:
    ```
    git clone https://gitlab.cern.ch/sft/lcgcmake.git
    export PATH=$PWD/lcgcmake/bin:$PATH
    ```

2. Configure a using the required `toolchain`, build all packages, and upload the binaries to EOS
    ```
    mkdir build; cd build
    lcgcmake config -v <lcgversion> --with-tarfiles -p <local-prefix>
    lcgcmake install all
    ...
    lcgcmake upload tarfiles --release
    ```

## Do a second installation using the uploaded tarfiles into a `local` /cvmfs

3. Install the uploaded binaries in a /cvmfs volume prepared in the local Mac. For this we need to unmount all the CVMFS volumes first and the 
    ```
    sudo umount /Users/Shared/cvmfs/sft.cern.ch
    sudo chown sftnight /Users/Shared/cvmfs/sft.cern.ch

    ...

    mkdir build;cd build
    lcgcmake config --version LCG_<lcgversion> \
                    --prefix /cvmfs/sft.cern.ch/lcg/releases \
                    --toolchain ../lcgcmake/cmake/toolchain/heptools-<lcgversion>.cmake
    lcgcmake install all
    ```
## Copy installation to CVMFS using the publisher node (cvmfs-sft.cern.ch)

4. Reserve by the [cvmfs-sft](https://epsft-jenkins.cern.ch/computer/cvmfs-sft/) node in Jenkins by putting it temporary offline.

5. Install in CVMFS by login to the installation node
    ```
    ssh sftnight@cvmfs-sft.cern.ch
    sudo -i -u cvsft
    cvmfs_server transaction sft.cern.ch

    cvmfs_rsync -av username@node:/cvmfs/sft.cern.ch/lcg/releases /cvmfs/sft.cern.ch/lcg
    cvmfs_rsync -av username@node:/cvmfs/sft.cern.ch/lcg/views /cvmfs/sft.cern.ch/lcg

    cvmfs_server publish
    exit
    exit
    ```
6. Don't forget to bring [cvmfs-sft](https://epsft-jenkins.cern.ch/computer/cvmfs-sft/) back online in Jenkins

## Restore CVMFS in the build node
7. Restore the CVMFS mount points
    ```
    sudo mount -t cvmfs sft.cern.ch /Users/Shared/cvmfs/sft.cern.ch
    ```


